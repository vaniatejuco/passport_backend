//passport config file

//use passport for local authentication
const passport = require("passport");

//load passportJWT for authentication of jwt
const passportJWT = require("passport-jwt");

//load JWT extraction for extracting web tokens later
const ExtractJWT = passportJWT.ExtractJwt;

//use local strategy for authentication
const LocalStrategy = require("passport-local").Strategy;

//Local strategy uses the email or username fields to check the user is valid

//use JWT strategy for authentication
const JWTStrategy = passportJWT.Strategy;

//include the user model
const UserModel = require("./models/User");

//include bcrypt library for encryption and decryption
const bcrypt = require("bcrypt-nodejs");

//import moment for date parsing and handling
const moment = require("moment");

//prep the user data that we want to store for the query
passport.serializeUser((user, done) => {
	done(null, user._id);
})

//how to filter or deserialize the user
passport.deserializeUser((user, done) => {
	done(null, user._id);
})

//configure passport.js to use the local srategy
passport.use( new LocalStrategy({ usernameField: "email"}, (email, password, done)=>{ 
	//query to check if the email exists in the db
	UserModel.findOne({'email' : email}).then(function (user){
		//uncomment this line to view the user that is being checked:
		//console.log(user);

		if(!user){
			return done(null, false, {"message": "invalid credentials"});
		}
		if (email == user.email) {
			//check if the password hash matches the stored password
			if(!bcrypt.compareSync(password, user.password)){
				//return invalid credentials if the password is incorrect
				return done(null, false, {"message": "Invalid credentials"});
			} else {
				//return a successful login if a match is found
				return done(null, user);
			}
		}

		//all other cases
		return done(null, false, {"message" : "Invalid credentials"});
	});


}))

//configure passport for JWT use

passport.use(new JWTStrategy({
	jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
	secretOrKey: 'secret-b25'
},
function(jwtPayload, cb){
	//find the user in the databse as needed
	return UserModel.findOne(jwtPayload.id)
	.then( function(user){
		return cb(null, user);
	})
	.catch( function(err){
		return cb(err);
	})
}
))