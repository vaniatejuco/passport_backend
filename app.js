
//include express
const express = require("express");
//mongoose for db connection

const mongoose = require("mongoose");
// cors for cross origin request
const cors = require("cors");

//mongoose

mongoose.connect("mongodb://localhost/sampledb");



//instantiate the backend
const app = express();

const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");
const passport = require("passport");

//require the passport file
require("./passport");


//use cors and body parser
app.use(cors());
app.use(bodyParser.json());

//port
const port = process.env.PORT || 3000;



//listen to port
app.listen(port, () => {
	console.log(`Server runnit at port ${port}`);
})

//registration
const reg = require("./routes/reg.js");
app.use("/register", reg);

//login
const auth = require("./routes/auth.js");
app.use("/auth", auth);


//dev
const dev = require("./routes/dev.js");
app.use("/admin", passport.authenticate("jwt", {session:false}), dev);


module.exports = router;

