const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const passport = require("passport");
const appPassport = require("../passport");

//login the user to the system
router.post("/login", (req, res, next) => {
	//authenticate using passport's local strategy
	passport.authenticate("local", {session:false}, (err, user, info) => {
		//if we are not able to validate, request is unauthorized
		if(err || !user){
			return res.status(400).json({
				"error": "Something is not right"
			})
		}

		console.log("success");

		req.login(user, {session:false}, (err) => {
			if(err){
				res.send(err);
			}
			//jwt.sign(payload, secretkey, [options, callback])
			const token = jwt.sign(user.toJSON(), "secret-b25", { expiresIn : '30m'});
			return res.status(200).json({
				"data" : {
					"user" : user,
					"token": token
				}
			})
		})

	}) (req, res)
})

//Logout
router.get("/logout", function(req, res){
	req.logout();
	res.json({
		"status": "logout",
		"msg": "Please Login again"
	})
})